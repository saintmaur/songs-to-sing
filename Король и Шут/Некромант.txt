2002 - Жаль, Нет ружья
		Некромант

		Ты говоришь, я - демон?
		Так и есть.
		Со мной не видать тебе удачи!
		Но так как моё дело - злая месть,
		Для демона - не может быть иначе.

!		<A5> (<Am>)
		Чем я заслужил судьбу несчастного изгоя?
!		<F5> (<F>)
		Мучил я себя вопросом "Кто я?"
!		<D5> (<Dm>)
		Мысли, что я - демон, часто выжить помогали,
>		E
		Люди же меня повсюду так и называли.

!		(Аккорды в припеве играются не просто, а с перебором след. ладов:..)
!		<Am>: струна 2- 1,0,1,0,1,0,3,1,0
!		<F>:  струна 1- 1,0,1,0,1,0,3,1,0
!		<Dm>: струна 1- 3,1,0,1,3,1,0,1
!		<E>:  струна 2- 3,1,0,3,1,0

>		Am
		И зловещая луна
>		F
		В мои муки влюблена,
>		Dm
		Отобрав души покой,
>		E
		Что ты делаешь со мной?

		Может ты мне дашь ответ,
		Почему весь белый свет,
		Обозлился на меня,
		Для чего родился я?

		"Церковь" (?) крестьян убить меня хотела,
		После, инквизиторы калечили мне тело,
		Все восстали против молодого некроманта,
		Сделав меня мученником моего таланта!

!		Припев

>		A5            G#m
		На половину - человек,
>		A5            A#5
		На половину - я мертвец,
		Таким останусь я навек,
		Я будто в... отец,
		Пора страданий - жизнь моя,
		Но выбор сделанный судьбой,
		Изменить не в силах я!
>		              A5
		Война с самим собой.

>		A5 F Dm Gm A5

!		Припев

		Люди мне враги, а ведь когда-то были братья..
		Я на всю округу наложил своё проклятье,
		Гибнут урожаи, а вокруг чума и голод
		И ветра залётные приносят ЖУТКИЙ ХОЛОД!

>		Am F Dm Gm A5