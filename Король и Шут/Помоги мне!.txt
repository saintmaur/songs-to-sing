1996 - Король и Шут, 2000 - Будь как дома, Путник
		Помоги мне!

>		      Fm#          A      Fm#
		Возвращаясь домой, парень шел по тропе.
		Над долиной болот зависала луна.
		И услышал он вдруг чей-то крик вдалеке,
		И пошел посмотреть, с кем случилась беда.
		И по мягкой земле он аккуратно ступал,
		Его коварная топь в себя пыталась втянуть.
		Недалеко от себя он старика увидал,
		Который крикнул ему: "Сынок не дай утонуть!"

Припев:		<      Fm#     H       Fm#>
		  Помоги мне, парень, выбраться!
>		      Fm#     H       Fm#
		  Помоги мне, парень, выбраться!
>		     D      H       Fm#
		  Бедняга рыдал, умолял.
		  Помоги мне, парень, выбраться!
		  Помоги мне, парень, выбраться!
		  Но парень с улыбкой стоял...

		И на корточки сев, с усмешкой парень сказал:
		"Не буду я тебе, дед, в твоей беде помогать,
		Как человек умирает, я вовек не видал,
		И вот вживую хочу я это щас увидать!"

!		Припев

-

		      <Fm#          A      Fm#>
(1)		Возвращаясь домой, парень шёл по тропе.
		Hад долиной болот зависала луна.
		И услышал он вдруг чей-то крик вдалеке,
		И пошёл посмотреть, с кем случилась беда.
		И по мягкой земле он аккуратно ступал,
		Его коварная топь в себя пыталась втянуть.
		Hедалеко от себя он старика увидал,
		Который крикнул ему: "Сынок не дай утонуть! "

		    <Fm#     H       Fm#>
Припев:		Помоги мне, парень, выбраться!
		    <Fm#     H       Fm#>
		Помоги мне, парень, выбраться!
		   <D      H       Fm#  A Fm# A>
		Бедняга рыдал, умолял.
		Помоги мне, парень, выбраться!
		Помоги мне, парень, выбраться!
		Hо парень с улыбкой стоял...

(2)		И на корточки сев, с усмешкой парень сказал:
		"Hе буду я тебе, дед, в твоей беде помогать,
		Как человек умирает, я вовек не видал,
		И вот вживую хочу я это щас увидать! "

!		Припев  } 2 раза