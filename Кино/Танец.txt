*НОЧЬ* (1985)
		Танец

>		 Em            C             G
		Танец на улице, танец на улице в дождь,
>		 Em                   C                   G
		Зонты, раскрываясь, звучат словно выстрелы ружей.
>		 Em               C           G  F
		Кто-то бежал, а кто-то остался здесь,
>		 Em                  C               G
		И тот, кто остался, шагает прямо по лужам.

>		    C          Cm7                    Em
		   Капли дождя лежат на лицах, как слезы,
>		   Hm   C                 Em
		   Текут по щекам словно слезы.
>		    C          Cm7                Em
		   Капли дождя лежат на лицах, как слезы,
>		   Hm   C                 Em
		   Ты знаешь теперь этот танец.

		Битые стекла, рваные брюки - скандал.
		К черту зонт, теперь уже все равно.
		Танец и дождь никогда не отпустят тебя,
		В их мокром обьятии не видно родное окно.

		   Капли дождя лежат на лицах, как слезы,
		   Текут по щекам словно слезы.
		   Капли дождя лежат на лицах, как слезы,
		   Ты знаешь теперь этот танец.

		Мокрые волосы взмахом ладони - назад,
		Закрыв свою дверь, ты должен выбросить ключ.
		И так каждый день, так будет каждый день,
		Пока не увидишь однажды небо без туч.

		   Капли дождя лежат на лицах, как слезы,
		   Текут по щекам словно слезы.
		   Капли дождя лежат на лицах, как слезы,
		   Ты знаешь теперь этот танец.