1997 - Бомбардировщики
		НЕЙЛОНОЫВОЕ СЕРДЦЕ
!		(Я. Френкель, И. Шаферан)

>		     Am             C            E
		Говорят, что будет сердце из нейлона
>		     Am              G            C
		Говорят, что двести лет стучать ему.
>		A7     Dm                Am
		Может, это по науке и резонно,
>		      E                    Am
		А по нашему, ребята, ни к чему!

		Кто-то слабого обидел и хохочет,
		Кто-то трусом оказался и скулит,
		А нейлоновое сердце не клокочет,
		А нейлоновое сердце не болит.

		Нет, ребята, не для нас определенно,
		Жить не слыша и не видя ничего.
		Даже если будет сердце из нейлона,
		Мы научим беспокоиться его!