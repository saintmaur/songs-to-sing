
		Lucky Bride?

Intro:		<Am  G  Em  Dm>
Verse:		<Am>
		Why?
>		G      Em         Dm
		Wanna tell me bye bye?
>		Am      G
		Shy
>		     Em        Dm
		To unzip and untie?
>		Am        G
		Butterfly lashes
>		       Em          Dm
		that flutter with passion
>		Am      G       Em     Dm
		Femme fatale fantastique

>		Am        G
		Dying to live
>		       Em       Dm
		in an elegant future
>		   Em
		You're casting a spell over me

Chorus:		< G      Dm>
		Look at My Lucky Bride!
>		Am     Dm      Am
		      Dreaming clever,
>		G                Dm
		  Everything we share,
>		                       Am
		You're gonna blow it all
>		                    Em
		Plastic cards and lives
>		   Dm
		Together

Verse 2:	Yum Yum
		Yoghurt spills on my arm
		Love
		E-mails for breakfast
		Beautiful Creatures
		Please do enjoy us
		We know how hard you may feel.
		You don't have to wait for inspiration
		But magic won't pay any bill