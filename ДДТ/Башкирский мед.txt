1983 - Компромисс
		Башкирский мед

Вступление:	<Am C G Em> } 4 раза

>		Em      Dm
		В Москве однажды я бывал.

		Куда  хотел, туда попал,
>		  C
		О чём мечтал всё заимел,

		Всё что хотел и не хотел.
>		  Em7
		Ему и слава и почёт,
>		     Dm
		Ты - козырь наш,
>		            Am
		Башкирский мёд.

Проигрыш:	<C G Em7 Am C G Em7>

		Вот там мой лучший друг живёт,
		Он для меня на всё пойдет,
		Умён, силен и очень смел.
		Когда бы я не захотел,
		Из под полы мне достаёт
		Индийский чай,
		Башкирский мёд.

Проигрыш:	<C G Em7 Am C G Em7>

		Кому-то в жизни не везёт,
		Мечта покоя не даёт.
		Не может он его достать.
		Все вон из рук, ни сесть ни встать.
		Бедняга морщась еле пьёт
		Грузиский чай,
		Башкирский мёд.