
		Вставай

Вступ:		<|Em|C|>

>		Em           C
(1)		Вставай, пий чай з молоком
		Молися на теплий душ
		Без тебе сьогодні сплять мільйони нових сердець
		Давай не можна стояти
		Навколо прямий ефір
		Тобі так мало залишилось
		Часу сказати всім
		В очі сказати їм
		В очі сказати їм.

>		Em      G            D
Приспів:	Вставай мила моя вставай
>		Hm           Em   G            D    Hm Em
		Мила моя вставай, мила моя вставай.
		Давай мила моя давай
		Мила моя давай, більшого вимагай.

Проігриш:	<|Em|C|>

(2)		Вставай – всім покажи
		Вистави де перший світ
		Твоя земля чекає
		На захід або на схід
		Заходу і на схід
		Заходу і на схід.

!		Приспів

Проігриш:	<|Em|G|D|Hm|>

!		Приспів

Кода:		<|Em|G|D|Hm|>
!		<Em> - кінець
