*ДЛЯ ТЕХ, КТО СВАЛИЛСЯ С ЛУНЫ* (1993)
		Республика

>		Dm# H

		Республика объединенных этажей.
		Гвардия ночи несет караул до утра.
		Определенность нужна лишь тому, кто хочет скорей,
		Здесь же никогда не поздно и никогда не пора.
		Республика!

		Республика объединенных этажей.
		Последний глоток перед выходом к роднику.
		Вязкий разговор прерван словом: "Налей!"
		Кажется, кто-то ищет нас где-то там, наверху.
		Ау! Республика!

>		Fm C#

		Республика объединенных этажей.
		Папиросный дым - хорошее средство от сна.
		Лестничный пролет, трон хмельных королей.
		Я помню дорогу на крышу, возможно, это моя беда.
		Республика!